#!/usr/bin/env ruby

require 'pry-byebug'
require 'optparse'
require 'ostruct'

VALID_INSTALLATIONS = %w(ce ee ee-review ee-test ce-security ee-security geo-primary geo-secondary)
LOCAL_URL           = 'gitlab.test'
POSTGRES_BIN_DIR    = '/Applications/Postgres.app/Contents/Versions/10/bin'

#------------------------------------------------------------------------------
def parse_options
  options = OpenStruct.new
  options.install = OpenStruct.new
  options.executable_name = File.basename($PROGRAM_NAME)

  option_parser = OptionParser.new do |opts|
    opts.banner  = "Install GDK and a specific GitLab instance"
    opts.separator ""
    opts.separator "Usage: #{options.executable_name} [options] [#{VALID_INSTALLATIONS.join('|')}]"

    opts.on('-p', '--base-port PORT_NUMBER', Integer, 'Beginning port to use for services') do |value|
      options.install.base_port = value
    end

    opts.on('-k', '--keep-git', 'Keep the same .git repository') do
      options.install.keep_git = true
    end

    opts.on('--keep-db', 'Keep the same database and project repositories') do
      options.install.keep_db = true
    end

    opts.on('--replace-gdk-yml', 'Use the latest gdk.yml (not one from a previous version)') do
      options.install.replace_gdk_yml = true
    end
  end
  option_parser.parse!

  options.target = ARGV.shift || 'ee'

  unless VALID_INSTALLATIONS.include?(options.target)
    raise ::OptionParser::InvalidArgument.new('An invalid installation target provided')
  end

  return options
end

#------------------------------------------------------------------------------
def installation_parameters(options)
  install = options.install

  case options.target
  when 'ce'
    install.base_port         ||= 5000
    install.directory         ||= 'ce'
    install.repo              ||= 'https://gitlab.com/gitlab-org/gitlab-foss.git'
    install.hostname          ||= install.directory
    install.env_mk            ||= "postgres_bin_dir := #{POSTGRES_BIN_DIR}"
  when 'ee'
    install.base_port         ||= 5100
    install.directory         ||= 'ee'
    install.repo              ||= 'https://gitlab.com/gitlab-org/gitlab.git'
    install.hostname          ||= install.directory
    # install.env_mk            ||= 'jaeger_server_enabled := false'
    install.env_mk            ||= "postgres_bin_dir := #{POSTGRES_BIN_DIR}"
  when 'ee-review'
    install.base_port         ||= 5200
    install.directory         ||= 'ee-review'
    install.repo              ||= 'https://gitlab.com/gitlab-org/gitlab.git'
    install.hostname          ||= install.directory
    # install.env_mk            ||= 'jaeger_server_enabled := false'
    install.env_mk            ||= "postgres_bin_dir := #{POSTGRES_BIN_DIR}"
  when 'ee-test'
    install.base_port         ||= 6100
    install.directory         ||= 'ee-test'
    install.repo              ||= 'https://gitlab.com/gitlab-org/gitlab.git'
    install.hostname          ||= install.directory
    install.env_mk            ||= "postgres_bin_dir := #{POSTGRES_BIN_DIR}"
  when 'geo-primary'
    install.base_port         ||= 5500
    install.directory         ||= 'geo/primary'
    install.repo              ||= 'https://gitlab.com/gitlab-org/gitlab.git'
    install.hostname          ||= 'primary'
    install.env_mk            ||= "postgres_bin_dir := #{POSTGRES_BIN_DIR}"
  when 'geo-secondary'
    install.base_port         ||= 5600
    install.directory         ||= 'geo/secondary'

    # we clone from the local primary repository.  This makes it easier to be
    # developing on the same branch between primary and secondary
    install.repo              ||= '../primary/gitlab'
    install.hostname          ||= 'secondary'
    install.env_mk            ||= "postgres_bin_dir := #{POSTGRES_BIN_DIR}"
  when 'ce-security'
    install.base_port         ||= 5800
    install.directory         ||= 'ce-security'
    install.repo              ||= 'https://dev.gitlab.org/gitlab/gitlabhq.git'
    install.hostname          ||= install.directory
    install.env_mk            ||= "postgres_bin_dir := #{POSTGRES_BIN_DIR}"
  when 'ee-security'
    install.base_port         ||= 5900
    install.directory         ||= 'ee-security'
    install.repo              ||= 'https://digitalmoksha@gitlab.com/gitlab-org/security/gitlab.git'
    install.hostname          ||= install.directory
    install.env_mk            ||= "postgres_bin_dir := #{POSTGRES_BIN_DIR}"
  end

  install.port              ||= install.base_port       # e.g. 5000
  install.gitlab_pages_port ||= install.base_port + 2   # e.g. 5002
  install.workhorse_port    ||= install.base_port + 3   # e.g. 5003
  install.registry_port     ||= install.base_port + 5   # e.g. 5005
  install.object_store_port ||= install.base_port + 6   # e.g. 5006
  install.webpack_port      ||= install.base_port + 9   # e.g. 5009
  install.directory_path    ||= File.expand_path(install.directory)
  install.backup_path         = "#{install.directory_path}.bak"
  install.gdk_yml_file        = "gdk.#{options.target}.yml"

  options.install = install
end

def replace_git(install)
  # replace the newly pulled gitlab/.git with the backed up one
  gitlab_path = File.join(install.directory_path, 'gitlab')
  backup_path = File.join(install.backup_path, 'gitlab', '.git')
  new_path    = File.join(gitlab_path, '.git')

  puts "--> Replacing new `gitlab/.git` with ...#{backup_path}"

  # first remove the newly pulled repo, then copy the old one into place
  FileUtils.rm_r(new_path, force: true)
  FileUtils.cp_r(backup_path, gitlab_path, preserve: true)
end

def replace_db(install)
  # replace the newly pulled gitlab/postgresql with the backed up one
  backup_path = File.join(install.backup_path, 'postgresql')
  new_path    = File.join(install.directory_path, 'postgresql')

  puts "--> Replacing new `postgresql` with ...#{backup_path}"

  # first remove the newly pulled repo, then copy the old one into place
  FileUtils.rm_r(new_path, force: true)
  FileUtils.cp_r(backup_path, install.directory_path, preserve: true)
end

def replace_repositories(install)
  # replace the newly pulled gitlab/repositories with the backed up one
  backup_path = File.join(install.backup_path, 'repositories')
  new_path    = File.join(install.directory_path, 'repositories')

  puts "--> Replacing new `gitlab/repositories` with ...#{backup_path}"

  # first remove the newly pulled repo, then copy the old one into place
  FileUtils.rm_r(new_path, force: true)
  FileUtils.cp_r(backup_path, install.directory_path, preserve: true)
end

def replace_secrets(install)
  # replace the newly generated config/secrets.yml with the old one
  gitlab_path = File.join(install.directory_path, 'gitlab')
  backup_file = File.join(install.backup_path, 'gitlab', 'config', 'secrets.yml')
  new_file    = File.join(gitlab_path, 'config', 'secrets.yml')

  puts "--> Replacing new `gitlab/config/secrets.yml` with ...#{backup_file}"

  FileUtils.cp(backup_file, new_file, preserve: true)
end

def copy_gdk_yml(install)
  new_gdk_file = File.join(install.directory_path, 'gdk.yml')

  if (install.keep_git || install.keep_db) && !install.replace_gdk_yml
    orig_gdk_file = File.join(install.backup_path, 'gdk.yml')
  end

  unless orig_gdk_file && File.exist?(orig_gdk_file)
    orig_gdk_file = File.join(Dir.pwd, install.gdk_yml_file)
  end

  return unless File.exist?(orig_gdk_file)

  puts "--> Copying `#{orig_gdk_file}` over"

  FileUtils.cp(orig_gdk_file, new_gdk_file, preserve: true)
end

#------------------------------------------------------------------------------

options = parse_options
install = installation_parameters(options)

puts "Create a GitLab #{options.target.upcase} instance"
puts '------------------------------------------------------------------------------'

if Dir.exist?(install.directory_path)
  if install.keep_git || install.keep_db || install.replace_gdk_yml
    if install.keep_git
      puts "- Keeping the exisiting GitLab .git repository, rather than pulling a fresh one."
      puts "  This will maintain your local branches, stashes, etc."
    end

    if install.keep_db
      puts "- Keeping the original postgresql and repositories directories."
    end
    if install.replace_gdk_yml
      puts "- Replacing the existing gdk.yml with #{install.gdk_yml_file}"
    end
    puts
    puts "The current installation will be renamed with `.bak`, a full-setup will be done,"
    puts "and then any items will be replaced with the original versions."
    puts ''
    puts 'Press <ENTER> to continue'
    $stdin.gets
  else
    puts "The target installation directory,"
    puts "\n  #{install.directory_path}\n\n"
    puts "already exists. You must first remove this directory to continue installation."
    puts ''
    puts "You can use the `--keep-git` option to keep the .git repository and replace"
    puts "the rest of the installation."
    exit(1)
  end
end

if install.keep_git || install.keep_db
  # rename the existing installation
  if Dir.exist?(install.backup_path)
    puts "`.bak` directory already exists. This could be from a previous installation"
    puts "or a failed install."
    puts ''
    puts "Do you wish to continue and use that directory? (y/n)"
    puts "  Answer 'y' if this is from a failed install and the `.bak` directory is correct."
    puts "  Answer 'n' to quit"
    answer = $stdin.gets.chomp

    exit(1) if answer.upcase != 'Y'

    # they want to continue - remove the failed install
    puts "--> Deleting directory...#{install.directory_path}"
    FileUtils.rm_r(install.directory_path, force: true)
  else
    puts "--> Renaming directory to...#{install.backup_path}"
    File.rename(install.directory_path, install.backup_path)
  end
end

puts "--> Initializing gdk in directory...#{install.directory_path}"
system("git clone https://gitlab.com/gitlab-org/gitlab-development-kit.git #{install.directory}")

copy_gdk_yml(install)

Dir.chdir("#{install.directory}") do
  if install.env_mk
    puts "--> Setting jaeger to use single instance"
    `echo #{install.env_mk} > env.mk`
  end

  puts "--> Bootstrapping...#{install.repo}"
  unless system("make bootstrap")
    puts 'FAILED bootstrapping GitLab repo'
    exit(1)
  end

  puts "--> Installing GitLab repo...#{install.repo}"
  puts "      gdk install gitlab_repo=#{install.repo}"
  puts "#{Dir.pwd}"

  unless system("cd #{Dir.pwd} && gdk install gitlab_repo=#{install.repo}")
    puts 'FAILED installing GitLab repo'
    exit(1)
  end
end

if install.keep_git || install.keep_db
  if install.keep_git
    replace_git(install)
  end

  if install.keep_db
    replace_db(install)
    replace_repositories(install)
    replace_secrets(install)
  end

  puts "--> Note: the `.bak` directory has NOT been deleted"
end

puts "--> If you plan on running multiple GDK installations, consider commenting out"
puts "     `prometheus_listen_addr` line in `#{install.directory}/gitaly/gitaly.config.toml`"
puts "     https://gitlab.com/gitlab-org/gitlab-development-kit/issues/482"
puts
puts "--> Installation finished!"
